﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameLoop : MonoBehaviour {

    public static GameLoop Instance { get; private set; }

    [Header("Debug")]
    [SerializeField] private bool _gameRunning;

    [Header("Spawn Controller")]
    [SerializeField] private SpawnController _spawnController;

    [Header("GameOver")]
    [SerializeField] private GameObject _gameOverPanel;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(this.gameObject);
    }

	// Use this for initialization
	void Start () {
        StartGame();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void StartGame()
    {
        _gameOverPanel.SetActive(false);
        _spawnController.StartSpawn();
    }

    public void GameOver()
    {
        _spawnController.StopSpawn();
        DestroyAllBoxes();
        _gameOverPanel.SetActive(true);
    }

    private void DestroyAllBoxes()
    {
        GameObject[] goodBoxesToDestroy = GameObject.FindGameObjectsWithTag("GoodBox");
        GameObject[] badBoxesToDestroy = GameObject.FindGameObjectsWithTag("BadBox");

        foreach (GameObject box in goodBoxesToDestroy)
        {
            Destroy(box);
        }

        foreach (GameObject box in badBoxesToDestroy)
        {
            Destroy(box);
        }
    }
}
