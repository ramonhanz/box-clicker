﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lifetime : MonoBehaviour {

    [Header("Object Lifetime")]
    [SerializeField] private int _lifeTime = 2;

	// Use this for initialization
	void Start () {
        Destroy(this.gameObject, _lifeTime);
    }
}
