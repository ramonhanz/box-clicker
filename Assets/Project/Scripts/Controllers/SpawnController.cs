﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnController : MonoBehaviour {

    public static SpawnController Instance { get; private set; }

    [Header("Debug")]
    [SerializeField] private bool _isSpawning;
    [SerializeField] private float _lastSpawn = 0f;
    [SerializeField] private bool _isWaiting;

    [Header("Spawn Positions")]
    [SerializeField] private List<Transform> _spawnPositions = new List<Transform>();

    [Header("Bad Box Rate")]
    [Range(0, 100)]
    [SerializeField] private int _badBoxRate = 30;
    [Range(0, 100)]
    [SerializeField] private int _timeBoxRate = 5;

    [Header("Spawn Times")]
    [SerializeField] private float _spawnDelay = 2f;
    [HideInInspector] private float _initialSpawnDelay;
    [SerializeField] private float _spawnDelayDecay = 0.15f;
    [SerializeField] private float _minSpawnDelay = 0.3f;

    [Header("Spawn Prefabs")]
    [SerializeField] private GameObject _goodBoxPrefab;
    [SerializeField] private GameObject _badBoxPrefab;
    [SerializeField] private GameObject _timeBoxPrefab;

    [Header("Particles To Spawn")]
    [SerializeField] private GameObject _goodParticle;
    [SerializeField] private GameObject _badParticle;
    [SerializeField] private GameObject _timeParticle;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(this);
    }

    // Use this for initialization
    void Start () {
        _initialSpawnDelay = _spawnDelay;
	}
	
	// Update is called once per frame
	void Update () {
        TryToSpawn();

        if (_isSpawning)
            _lastSpawn -= Time.deltaTime;
	}

    private void TryToSpawn()
    {
        if (_isSpawning && _lastSpawn <= 0f)
        {
            StartSpawningBox();
        }
    }

    /// <summary>
    /// Inicia o spawn das caixas
    /// </summary>
    private void StartSpawningBox()
    {
        if (IsATimeBox())
        {
            SpawnBox(_timeBoxPrefab, GetSpawnPosition());
        }
        else
        {
            if (IsAGoodBox())
                SpawnBox(_goodBoxPrefab, GetSpawnPosition());
            else
                SpawnBox(_badBoxPrefab, GetSpawnPosition());
        }

        ReduceSpawnDelay();
        _lastSpawn = _spawnDelay;
    }

    /// <summary>
    /// Spawn uma caixa
    /// </summary>
    /// <param name="objectToSpawn"></param>
    /// <param name="transformForSpawn"></param>
    private void SpawnBox(GameObject objectToSpawn, Transform transformForSpawn)
    {
        Instantiate(objectToSpawn, transformForSpawn);
    }

    /// <summary>
    /// Retorna o transform escolhido para spawn
    /// </summary>
    /// <returns></returns>
    private Transform GetSpawnPosition()
    {
        int transformIndex = Random.Range(0, _spawnPositions.Count);

        return _spawnPositions[transformIndex];
    }

    /// <summary>
    /// Define se a caixa será boa ou má
    /// </summary>
    /// <returns></returns>
    private bool IsAGoodBox()
    {
        return (Random.Range(0, 100) > _badBoxRate);
    }

    /// <summary>
    /// Retorna verdadeiro se a caixa escolhida é uma TimeBox
    /// </summary>
    /// <returns></returns>
    private bool IsATimeBox()
    {
        return (Random.Range(0, 100) < _timeBoxRate);
    }

    /// <summary>
    /// Diminui o tempo de delay para o spawn
    /// </summary>
    private void ReduceSpawnDelay()
    {
        if (_spawnDelay - _spawnDelayDecay < _minSpawnDelay)
            _spawnDelay = _minSpawnDelay;
        else
            _spawnDelay -= _spawnDelayDecay;
    }

    #region PARTICLES
    /// <summary>
    /// Retorna qual particula sera criada
    /// </summary>
    /// <param name="Tag"></param>
    /// <returns></returns>
    public GameObject GetParticleToSpawn(string Tag)
    {
        switch (Tag)
        {
            case "GoodBox":
                return _goodParticle;
            case "BadBox":
                return _badParticle;
            case "TimeBox":
                return _timeParticle;

            default:
                Debug.LogError("This type of box is unknown: " + this.gameObject.tag);
                return null;
        }
    }
    #endregion

    #region PUBLIC METHODS
    /// <summary>
    /// Inicia o spawn
    /// </summary>
    public void StartSpawn()
    {
        _spawnDelay = _initialSpawnDelay;
        _lastSpawn = 0f;
        _isSpawning = true;
    }

    /// <summary>
    /// Para o spawn
    /// </summary>
    public void StopSpawn()
    {
        _isSpawning = false;
    }
    #endregion
}
