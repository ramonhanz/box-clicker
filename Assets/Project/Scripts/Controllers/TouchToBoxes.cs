﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchToBoxes : MonoBehaviour {

    [Header("Tags To Compare")]
    [SerializeField] private string _goodBoxTag = "GoodBox";
    [SerializeField] private string _badBoxTag = "BadBox";

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        TryToTouchBoxes();
    }

    private void TryToTouchBoxes()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 1000))
            {
                Debug.DrawLine(ray.origin, hit.point);
                Debug.Log("Hitting: " + hit.collider.gameObject.name);

                if (hit.collider.gameObject.CompareTag(_goodBoxTag))
                    OnGoodBoxHit();
                else if (hit.collider.gameObject.CompareTag(_badBoxTag))
                    OnBadBoxHit();
            }
        }
    }

    private void OnGoodBoxHit()
    {
        Debug.Log("Hitted some Good Box");
    }

    private void OnBadBoxHit()
    {
        Debug.Log("Hitted some Bad Box");
    }
}
