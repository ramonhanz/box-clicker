﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxBehaviour : MonoBehaviour {

    [Header("Points To Earn")]
    [SerializeField] private int _pointsToRecieve = 1;

    [Header("VFX Lifetime")]
    [SerializeField] private int _lifeTime = 1;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnMouseDown()
    {
        Debug.Log("Hitting: " + gameObject.name);
        if (this.gameObject.gameObject.CompareTag("GoodBox"))
            OnGoodBoxHit();
        else if (this.gameObject.gameObject.CompareTag("BadBox"))
            OnBadBoxHit();
        else if (this.gameObject.gameObject.CompareTag("TimeBox"))
            OnTimeBoxHit();
    }

    private void OnGoodBoxHit()
    {
        Debug.Log("Hitted some Good Box");

        SpawnParticle();
        transform.DetachChildren();
        ScoreSign.Instance.AddScore(_pointsToRecieve);
        KillBox();
    }

    private void OnBadBoxHit()
    {
        Debug.Log("Hitted some Bad Box");

        SpawnParticle();
        transform.DetachChildren();
        KillBox();
        GameLoop.Instance.GameOver();
    }

    private void OnTimeBoxHit()
    {
        Debug.Log("Hitted some Time Box");

        SpawnParticle();
        transform.DetachChildren();

        GameObject[] badBoxesToDestroy = GameObject.FindGameObjectsWithTag("BadBox");
        foreach (GameObject box in badBoxesToDestroy)
        {
            Destroy(box);
        }
        KillBox();
    }

    private void KillBox()
    {
        Destroy(this.gameObject);
    }

    private void SpawnParticle()
    {
        GameObject particle = Instantiate(SpawnController.Instance.GetParticleToSpawn(this.gameObject.tag), transform.position, Quaternion.identity);
        Destroy(particle, _lifeTime);
    }
}
