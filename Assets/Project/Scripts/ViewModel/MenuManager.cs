﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour {

    [Header("Scene To Load")]
    [SerializeField] private int _sceneIndex;
    [SerializeField] private bool _checkForButtons = true;

    [Header("Buttons")]
    [SerializeField] private Button _startButton;
    [SerializeField] private Button _exitButton;

    void Awake()
    {
        if(_startButton != null)
        {
            _startButton.onClick.RemoveAllListeners();
            _startButton.onClick.AddListener(() => OnStartButtonClicked());
        }
        else if(_checkForButtons)
        {
            Debug.LogError("StartButton is not defined, please drag the button in editor!");
        }

        if (_exitButton != null)
        {
            _exitButton.onClick.RemoveAllListeners();
            _exitButton.onClick.AddListener(() => QuitGame());
        }
        else if (_checkForButtons)
        {
            Debug.LogError("QuitButton is not defined, please drag the button in editor!");
        }
    }

    /// <summary>
    /// Executado no click do botão de start
    /// </summary>
    private void OnStartButtonClicked()
    {
        SceneManager.LoadScene(_sceneIndex);
    }

    /// <summary>
    /// Carrega uma outra cena, passada por parametro
    /// </summary>
    /// <param name="scene"></param>
    public void LoadSomeScene(int scene)
    {
        SceneManager.LoadScene(scene);
    }

    /// <summary>
    /// Sai do jogo
    /// </summary>
    private void QuitGame()
    {
        Application.Quit();
    }
}
