﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreSign : MonoBehaviour {

    public static ScoreSign Instance { get; private set; }

    [Header("Current Score")]
    [SerializeField] private int _currentScore;

    [Header("Score Text")]
    [SerializeField] private Text _scoreTextUI;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(this.gameObject);

        if (_scoreTextUI == null)
            Debug.Log("Text for Score Interface not defined, drag some text to editor.");
    }

    /// <summary>
    /// Soma score de acordo com o valor passado
    /// </summary>
    /// <param name="amountToAdd"></param>
    public void AddScore(int amountToAdd)
    {
        _currentScore += amountToAdd;

        UpdateInterface();
    }

    /// <summary>
    /// Atualiza o score na interface do jogo
    /// </summary>
    private void UpdateInterface()
    {
        _scoreTextUI.text = GetScoreAsDualDigit(_currentScore);
    }

    /// <summary>
    /// Retorna o score como um valor de no mínimo 2 dígitos
    /// </summary>
    /// <param name="score"></param>
    /// <returns></returns>
    private string GetScoreAsDualDigit(int score)
    {
        if (score < 10)
            return "0" + score.ToString();
        else
            return score.ToString();
    }
}
